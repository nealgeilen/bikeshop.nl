var ChatBot = {
    Actions : {
        container : $(".chatbot-container"),
        Hide: function (){
            this.container.find(".frame").slideUp(400,function (){
                ChatBot.Actions.container.find(".open-btn").show();
            });
        },
        Show: function (){
            this.container.find(".frame").slideDown();
            this.container.find(".open-btn").hide();
        }
    }
}