<footer class="footer">
    <div class="text-center">Bikeshop  © {$smarty.now|date_format:"Y"}. Alle rechten voorbehouden.</div>
</footer>
<noscript>
    <div class="noscript">
        <div class="alert alert-danger">
            U heeft Javascript niet ingeschakeld voor deze webiste. Hierdoor zal de webiste niet volledig functioneel zijn.
            <hr/>
            Als u deze website wilt gebruiken is het raadzaam javascript in te schakelen. U kunt <a>hier</a> vinden hoe dat moet doen.
        </div>
    </div>
</noscript>