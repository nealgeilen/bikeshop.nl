<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/vendor/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="/assets/css/bikeshop.css">
<title>Bikeshop.nl</title>
