<!DOCTYPE html>
<html lang="en">

<head>
    {include "./Sections/body_head.tpl"}
</head>


<body id="page-top">

{include "./Sections/nav.tpl"}

{include "./items/BikeCard.tpl"}
{include "./items/Bikes.tpl"}
{include "./items/repair.tpl"}
{include "./items/contact.tpl"}

<div class="chatbot-container">
    <div class="frame" style="display: none;">
        <button class="gradient-background btn close-btn btn-sm text-white" onclick="ChatBot.Actions.Hide()">
            <i class="fas fa-times"></i>
        </button>
        <iframe
                allow="microphone;"
                width="350"
                height="430"
                src="https://console.dialogflow.com/api-client/demo/embedded/8a34f756-720f-4ae8-a628-b3c2f39e9e04">
        </iframe>
    </div>

    <button class="gradient-background btn open-btn" onclick="ChatBot.Actions.Show()">
        <i class="far fa-comments"></i>
    </button>
</div>

{include "./Sections/footer.tpl"}

</body>

{include "./Sections/body_end.tpl"}

</html>


