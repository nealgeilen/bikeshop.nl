<section class="repair-section" id="shop">
    <div class="cover-container cover-container-right">
        <div class="cover-card">
            <div class="cover-content">
                <h1>Maak een afspraak</h1>
                <form class="form-row" form="contact" novalidate="novalidate">
                    <div class="form-group col-6">
                        <label>Voornaam</label>
                        <input type="text" name="Voornaam" class="form-control" required="">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-6">
                        <label>Achternaam</label>
                        <input type="text" name="Achternaam" class="form-control" required="">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-6">
                        <label>Email</label>
                        <input type="email" name="Email" class="form-control" required="">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-6">
                        <label>Datum</label>
                        <input type="datetime-local" name="Email" class="form-control" required="">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-12">
                        <label>Bericht</label>
                        <textarea name="Bericht" class="form-control" required=""></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-12">
                        <input type="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>