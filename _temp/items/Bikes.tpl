<section class="bikes-section gradient-background" id="bikes">
    <h1 class="text-center text-white p-3">Nieuwe fietsen</h1>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="card m-4">
                    <div class="card-body">
                        <img src="https://cdn.webshopapp.com/shops/252710/files/322078013/2026x1188x2/image.jpg">
                        <h3>Montreal Male Watt</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card m-4">
                    <div class="card-body">
                        <img src="https://www.forzafietsen.nl/wp-content/uploads/2019/05/DSC_0110_960x640.png">
                        <h3>Vito Max</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card m-4">
                    <div class="card-body">
                        <img src="https://www.fietsvoordeelshop.nl/local_resources/image/thumb_w64_Pegsus-piazza212020-Dames-Blauw-PegasusHybrideFietsen.png">
                        <h3>Pegasus Piazza 21</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>