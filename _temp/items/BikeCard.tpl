<section class="bike-section">
    <div class="cover-container">
        <div class="cover-card">
            <div class="cover-content">
                <img src="/assets/images/bike-1.png" alt="" class="box-shadow">
                <h2>Nieuwe Gaint fiets!</h2>
            </div>
        </div>
    </div>
</section>